#!/bin/bash

# check if there are any data to analyze 
if [[ "$(ls data/background)" = "" && "$(ls data/signal)" = "" ]]
then
    echo
    echo "No data were found."
    echo "Please add .root files to appropriate folders in the \"data\" directory."
    echo
    exit 1
fi

# generate relevant code 
cd build
python setupcuts.py setup > ../src/setup.cc
python setupcuts.py header > ../src/setup.h
cd .. 

echo 
case "$1" in
    --code)
	echo "setup.cc and setup.h files generated." 
	;;
    --rootout)
	cd src
	root -l -q wggSelection.cc
	cd ..
	;;
    --csv)
	if [ "$(ls output/eventsPassed.root)" = "" ]
	then
	    echo "Warning: there are no files in the output directory." >&2
	    echo "No .csv file has been generated." >&2
	    echo "Try running \"./run.sh --rootout\" first." >&2
	    echo
	    exit 1 
	fi
	echo "Generating .csv file with data..." 
	python build/exportData.py > src/exportData.cc
	cd src
	root -l -q exportData.cc
	echo "Done!"
	cd .. 
	;;	
    --plot) 
	if [ "$2" = "" ]
	then
	    echo "Nothing happened; option(s) required." >&2
	    echo "Use \"./run.sh --help\" for information on options." >&2
	    echo 
	    exit 1 
	fi 
	shift
	python build/makePlots.py header > src/plots.h 
	while getopts :is opt
	do
	    case $opt in
		i)
		    if [ "$(ls output/eventsPassed.root)" = "" ] 
		    then
			echo "Warning: there are no files in the output directory." >&2
			echo "No new .pdf files will be generated." >&2
			echo "Try running \"./run.sh --rootout\" first." >&2 
			echo 
		    fi
		    python build/makePlots.py indl > src/plots.cc
		    cd src
		    echo "Plotting individual histograms..." 
		    root -l -q plots.cc
		    echo "Done!"
		    cd ..
		    ;;
		s)
		    if [ "$(ls output/eventsPassed.root)" = "" ]
                    then
                        echo "Warning: there are no files in the output directory." >&2
 			echo "No new .pdf files will be generated." >&2
			echo "Try running \"./run.sh --rootout\" first." >&2
			echo 
                    fi
		    python build/makePlots.py super > src/plots.cc
		    cd src
		    echo "Plotting superimposed histograms..." 
		    root -l -q plots.cc
		    echo "Done!"
		    cd ..
		    ;; 
		\?)
		    echo "Invalid option(s) ignored: -$OPTARG." >&2
		    echo "Use \"./run.sh --help\" for information on options." >&2
		    ;;
	    esac
	    echo 
	done
	;;
    --neunet)
	if [ "$(ls output/eventsPassed.csv)"  = "" ]
	then
	    echo "Warning: there are no files in the output directory." >&2
	    echo "No .csv file has been generated." >&2
	    echo "Try running \"./run.sh --csv\" first." >&2
	    echo
	    exit 1
	fi
	if [ "$#" -lt 4 ]
	then
	    echo "Not enough parameters." >&2
	    echo "Try running \"./run.sh --neuralnet [alpha iters] [weight iters] [kin var(s)]\" instead." >&2
	    echo
	    exit 1
	fi 
	shift 
	python src/predictBackground.py $*
	;;
    --help) 
	echo "Run the program by using:"
	echo "./run.sh [--help] [--code] [--rootout] [--csv] [--plot [-is]] [--neunet [alpha iters] [weight iters] [kinvar(s)]]"
	echo
	echo
	echo "--help    you should know what this does"
	echo "--code    only generates code for setup.cc and setup.h"
	echo "--rootout produces ROOT files in \"output\" directory with a tree containing data on all events passing selection cuts"
	echo "--csv     generates a .csv file with data on all events passing selection cuts in \"output\" directory from a ROOT file"
	echo "--plot"
	echo "       -i generates .pdf files for individual histograms"
	echo "       -s generates .pdf files for superimposed histograms"
	echo "--neunet  uses a neural network to attempt to create a connection between kinematic variables and background events"
	echo "       [alpha iters]  the number of iterations when calculating the learning rate"
	echo "       [weight iters] the number of iterations when calculating the weights relating input variables to output data"
	echo "       [kin var(s)]   any kinematic variables that are to be included in the analysis, separated by spaces" 
	;;
    *) echo "Invalid arguments, or no arguments provided."
	echo "Use \"./run.sh --help\" for information on arguments and options."
	;;
esac
echo 

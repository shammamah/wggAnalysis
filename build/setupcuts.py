import os 
import sys

# we might be in a subdirectory
endpath = os.getcwd()[:os.getcwd().index("wggAnalysis")+11]

bgpath = "%s/data/background/"%(endpath)
sigpath = "%s/data/signal/"%(endpath) 
outpath = "%s/output/"%(endpath) 
buildpath = "%s/build/"%(endpath) 
sig = os.listdir(sigpath)
bg = os.listdir(bgpath)

tChains = [] 
slices_bg = []
slices_sig = []

histos = []
criteria = {}
passtree = {}

nbins = 0

setupCode = []

setupCode.append(" "*4 + "// import data\n")

# loop through all of the files in the background directory, import 
# their data into TChain objects 
for f in bg:
    if(os.path.isdir(bgpath+f)):
        slicedata = os.listdir(bgpath+f)
        begin = 0
        # look for the point at which the slice low/high pT are defined
        while(f[begin] != '_'):
            begin = begin + 1 
        pTslice = (f[begin+1:len(f)]).split('_')
        slices_bg.append(pTslice)
        try: # for slices like the one for 140+ GeV, only a lower bound is defined 
            tChains.append(" "*4 + "TChain bg_%s_%s(\"recoTree\");"%(pTslice[0],pTslice[1]))
        except: 
            tChains.append(" "*4 + "TChain bg_%s(\"recoTree\");"%pTslice[0])
        for d in slicedata:
            try:
                setupCode.append((" "*4 + "bg_%s_%s.Add(\""%(pTslice[0],
                                          pTslice[1]))+bgpath+f+"/"+d+"\");")
            except:
                setupCode.append((" "*4 + "bg_%s.Add(\""%(pTslice[0]))+bgpath+f+"/"+d+"\");")
        setupCode.append("\n\n")

# same process as above, but looping through signal data 
for f in sig:
    if(os.path.isdir(sigpath+f)):
        slicedata = os.listdir(sigpath+f)
        begin = 0
        while(f[begin] != '_'):
            begin = begin + 1
        pTslice = (f[begin+1:len(f)]).split('_')
        slices_sig.append(pTslice)
        try:
            tChains.append(" "*4 + "TChain sig_%s_%s(\"recoTree\");"%(pTslice[0], pTslice[1]))
        except:
            tChains.append(" "*4 + "TChain sig_%s(\"recoTree\");"%(pTslice[0]))
        for d in slicedata:
            try:
                setupCode.append((" "*4 + "sig_%s_%s.Add(\""%(pTslice[0],
                                               pTslice[1]))+sigpath+f+"/"+d+"\");")
            except:
                setupCode.append((" "*4 + "sig_%s.Add(\""%(pTslice[0]))+sigpath+f+"/"+d+"\");")
        setupCode.append("\n\n")

# get all of the histograms that are to be generated, as well as
# the axis titles
with open("%s/config/histos.txt"%buildpath) as f:
    lines = list(f.readlines())
    histos = [line.strip('\n').split(',') for line in lines if "//" not in line and "nbins" not in line and len(line.split(',')) == 2]
    nbins = [line.strip('\n').split(',')[1] for line in lines if "nbins" in line and "//" not in line][0]

# define all relevant branches as they will be named in the macro, 
# the branch names to which they correspond, and the data types 
# that they represent

with open("%s/config/criteria.txt"%buildpath) as f:
    lines = list(f.readlines())
    for line in lines:
        if("//" in line):
            continue
        l = line.strip('\n').split(',')
        if(len(l)!=3):	
            continue
	criteria[l[0]] = [l[1],l[2]]

with open("%s/config/cutpassdata.txt"%buildpath) as f:
    lines = list(f.readlines())
    for line in lines:
        if("//" in line or line == '\n'):
            continue
        l = line.strip('\n').split(',')
        if(len(l)!=3):
            continue
        passtree[l[0]] = [l[1],l[2]]

        
# setup mode generates the setup.cc file 
if(sys.argv[1] == "setup"):
    print("#include \"setup.h\"\n")
    print("void setup(){")
    # this is the TTree we'll use to store all events 
    # that pass these cuts
    print("\n"+" "*4+"// storage for events that pass all cuts\n")
    print(" "*4 + "ep = new TFile(\"%seventsPassed.root\",\"recreate\");"%outpath)
    print(" "*4 + "evpass = new TTree(\"eventsPassed\", \"Events Passed\");\n")
    for p in passtree:
        if("vector" in passtree[p][1]):
            # get the vector type 
            vt = passtree[p][1][passtree[p][1].find('<')+1:passtree[p][1].find('>')]
            print(" "*4 + "evpass->Branch(\"%s\", \"vector<%s>\", &%s);"%(passtree[p][0],vt,p))
        else: 
            print(" "*4 + "evpass->Branch(\"%s\", &%s);"%(passtree[p][0],p))
    # slices 
    print("\n\n" + " "*4 + "// slices\n") 
    for s in slices_bg:
        try:
            print(" "*4 + "l.Add(&bg_%s_%s);"%(s[0],s[1]))
        except:
            print(" "*4 + "l.Add(&bg_%s);"%(s[0]))
    for	s in slices_sig:	
        try:
            print(" "*4 + "l.Add(&sig_%s_%s);"%(s[0],s[1]))
        except:
            print(" "*4 + "l.Add(&sig_%s);"%(s[0]))
    print('\n')
    # print the rest of the code (TChains, etc.) 
    for s in setupCode:
        print(s)
    print("}\n")

# header mode generates the setup.h file 
elif(sys.argv[1] == "header"):
    print("#ifndef SETUP_H\n#define SETUP_H\n")
    print("#include <vector>\n")
    print("using namespace std;\n")
    print(" "*4 + "// for iterating through slices")
    print(" "*4 + "TList l;")
    print(" "*4 + "TIter next(&l);\n\n")
    # names of all slices 
    print(" "*4 + "// slices")
    print(" "*4 + "const string slicenames[] = {")
    for s in slices_bg:
        try:
            print(" "*4 + "\"background_%s_%s\","%(s[0],s[1]))
        except IndexError:
            print(" "*4 + "\"background_%s\","%(s[0]))
    for s in slices_sig:
        try:
            print(" "*4 + "\"signal_%s_%s\","%(s[0],s[1]))
        except IndexError:
            print(" "*4 + "\"signal_%s\","%(s[0]))
    print(" "*4 + "};\n\n")
    # declaring new TTree for events that pass all cuts
    print(" "*4 + "// storage of events that pass")
    print(" "*4 + "TFile *ep;")
    print(" "*4 + "TTree *evpass;\n\n")
    # chaining files together 
    print(" "*4 + "// combined data for each slice")
    for c in tChains:
        print(c)
    print("\n")
    # defining relevant variables 
    print(" "*4 + "// branches (")
    for item in criteria: 
        print(" "*4 + "%s %s;"%(criteria[item][1], item))
    for item in passtree:
        if(item in criteria):
            continue 
        print(" "*4 + "%s %s;"%(passtree[item][1], item))
    print('\n')
    # this text file contains all of the relevant cuts that 
    # are made/any important constants 
    with open("%s/config/cutparams.txt"%buildpath) as f:
        print (" "*4 + "// cut parameters") 
        for line in f.readlines():
            if("//" in line or line == '\n'):
                continue 
            print(" "*4 + line[:-1])
    print('\n')
    print("#endif")

import os
import sys

# we might be in a subdirectory
endpath = os.getcwd()[:os.getcwd().index("wggAnalysis")+11] 

# defines where to find output, build, and source files 
outpath = "%s/output"%(endpath)
buildpath = "%s/build"%(endpath) 
srcpath = "%s/src"%(endpath)

eventsPassed = "%s/eventsPassed.root"%outpath

# define the size of the histograms 
lenx = 1800
leny = 1200

# data about the two leading
# photons and the leading
# electron per event 
histos_egammagamma = []

# order in which the vectors are filled in 
vectorOrder = []

# other data about the event
# (e.g., invariant masses) 
histos = []

# all relevant branches 
branches = []

# get all desired histograms 
with open("%s/config/histos.txt"%(buildpath)) as f:
    lines = list(f.readlines())
    histos = [line.strip('\n').split(',') for line in lines if '//' not in line and "nbins" not in line and len(line.strip('\n').split(',')) == 3]
    nbins = [line.strip('\n').split(',')[1] for line in lines if '//' not in line and "nbins" in line][0]
    f.close()

with open("%s/config/histos_egammagamma.txt"%(buildpath)) as f:
    lines = list(f.readlines())
    histos_egammagamma = [line.strip('\n').split(',') for line in lines if '//' not in line and len(line.strip('\n').split(',')) == 2]
    vectorOrder = [line.strip('\n').split(':')[1].split(',') for line in lines if '//' not in line and "VECTOR_ORDER" in line][0]
    f.close()

# get all relevant branches 
with open("%s/config/cutpassdata.txt"%(buildpath)) as f:
    lines = list(f.readlines())
    egammagamma = [line.strip('\n').split(',')[:2] for line in lines if '//' not in line and ("photon" in line or "electron" in line)]
    branches = [line.strip('\n').split(',') for line in lines if '//' not in line and "VECTOR_ORDER" not in line and len(line.strip('\n').split(',')) == 3]

# list of plots to make 
plots =	["bg_events", "sig_events"]

if(sys.argv[1] == "indl" or sys.argv[1] == "super"):
    print("#include \"plots.h\"")
    print
    print("void plots(){ ")

    # turn off verbose output   
    print(" "*4 + "gErrorIgnoreLevel = kFatal;")

    # prevent histograms from displaying
    print(" "*4 + "gROOT->SetBatch(kTRUE);")
    print
    
    # get tree of events that have passed  
    print(" "*4 + "TFile *ep = new TFile(\"%s\");"%eventsPassed)
    print(" "*4 + "TTree *evpass = (TTree *)ep->Get(\"eventsPassed\");")
    print

    # initialize canvas and bins
    print(" "*4 + "TCanvas *c = new TCanvas;")
    print
    print(" "*4 + "int nbins = %s;\n"%nbins)

    # set up branches
    for b in branches:
        print(" "*4 + "evpass->SetBranchAddress(\"%s\",&%s);"%(b[1],b[0]))
    print

    # only create one canvas if we want the superimposed plots 
    if(sys.argv[1] == "super"): 
            print(" "*4 + "c = new TCanvas(\"super\", \"super\", %d, %d);"%(lenx, leny))
            print(" "*4 + "c->SetRightMargin(0.09);")
            print(" "*4 + "c->SetLeftMargin(0.15);")
            print(" "*4 + "c->SetBottomMargin(0.10);")
            print(" "*4 + "c->SetTopMargin(0.25);")
            print(" "*4 + "c->cd();")
            print 

    for p in plots:

        # create two canvases if we want individual plots 
        if(sys.argv[1] == "indl"): 
            print(" "*4 + "c = new TCanvas(\"%s\", \"%s\", %d, %d);"%(p, p, lenx, leny))
            print(" "*4 + "c->SetRightMargin(0.09);")
            print(" "*4 + "c->SetLeftMargin(0.15);")
            print(" "*4 + "c->SetBottomMargin(0.20);")
            print(" "*4 + "c->SetTopMargin(0.15);")
            print(" "*4 + "c->cd();")
            print

        # declare all histograms 
        for h in histos:
            print(" "*4 + "%s = new TH1F(\"%s\", \"%s\", nbins, 0, 0);"%(h[2].replace(' ',''),h[2],h[2]))
            print(" "*4 + "%s->Sumw2();"%(h[2].replace(' ','')))
        print
        for h_egg in histos_egammagamma:
            for egg in egammagamma:
                th1fname = h_egg[0] + " " + egg[1].replace('_',' ').title()
                print(" "*4 + "%s = new TH1F(\"%s\", \"%s\", nbins, 0, 0);"%(th1fname.replace(' ',''), th1fname, th1fname))
                print(" "*4 + "%s->Sumw2();"%(h[2].replace(' ','')))
        print 

        # fill all histograms 
        print(" "*4 + "for (int i = 0; i < evpass->GetEntries(); i++){")
        print(" "*8 + "evpass->GetEntry(i);")
        print(" "*8 + "if(bg_event){ ")
        if("bg" not in p):
            print(" "*12 + "continue; \n" + " "*8 + "}\n" + " "*8 + "else {")
        print(" "*12 + "double wt = mcEventWeight*pileupWeight;");
        for h in histos:
            print(" "*12 + "%s->Fill(%s, wt);"%(h[2].replace(' ',''), h[0]))
        for h_egg in histos_egammagamma:
            for egg in egammagamma:
                th1fname = h_egg[0] + " " + egg[1].replace('_',' ').title()
                print(" "*12 + "%s->Fill(%s->at(%s), wt);"%(th1fname.replace(' ',''), egg[0], vectorOrder.index(h_egg[0])))
        print(" "*8 + "}")
        print(" "*4 + "}")
        print

        # if we want the superimposed histograms, then we need 
        # to save them (and tag them with the source of the event)
        if(sys.argv[1] == "super"): 
            for h in histos: 
                print(" "*4 + "TH1F *%s_%s = (TH1F *)%s->Clone();"%(p, h[2].replace(' ',''), h[2].replace(' ','')))
            for h_egg in histos_egammagamma: 
                for egg in egammagamma: 
                    th1fname = h_egg[0] + " " + egg[1].replace('_',' ').title()
                    print(" "*4 + "TH1F *%s_%s = (TH1F *)%s->Clone();"%(p, th1fname.replace(' ',''), th1fname.replace(' ','')))
            print 
        
        # create all individual plots  
        if(sys.argv[1] == "indl"): 
            print(" "*4 + "c->Print(\"%s/plots/%s.pdf[\",\"pdf\");"%(endpath,p))
            print
            # rebin and print to .pdf file 
            for h in histos:
                print(" "*4 + "%s->GetXaxis()->SetTitle(\"%s\");"%(h[2].replace(' ',''), h[1]))
                print(" "*4 + "grp = 1.5*(1 + (int)log2(%s->GetEntries()));"%(h[2].replace(' ','')))
                print(" "*4 + "h = (TH1F *)%s->Clone();"%(h[2].replace(' ','')))
                print(" "*4 + "h->Rebin(grp)->Draw();")
                print(" "*4 + "c->Print(\"%s/plots/%s.pdf\",\"Title:%s\");"%(endpath, p, h[2]))
                print 
            for h_egg in histos_egammagamma:
                for egg in egammagamma:
                    th1fname = h_egg[0] + " " + egg[1].replace('_',' ').title()
                    print(" "*4 + "%s->GetXaxis()->SetTitle(\"%s\");"%(th1fname.replace(' ',''), h_egg[1]))
                    print(" "*4 + "grp = 1.5*(1 + (int)log2(%s->GetEntries()));"%(th1fname.replace(' ','')))
                    print(" "*4 + "h = (TH1F *)%s->Clone();"%(th1fname.replace(' ','')))
                    print(" "*4 + "h->Rebin(grp)->Draw();")
                    print(" "*4 + "c->Print(\"%s/plots/%s.pdf\",\"Title:%s %s\");"%(endpath, p, h_egg[0], egg[1].replace('_',' ').title()))
                    print 
            print(" "*4 + "c->Print(\"%s/plots/%s.pdf)\", \"pdf\");"%(endpath, p))
            print 
            print(" "*4 + "c->Clear();")


    # create superimposed plots 
    if(sys.argv[1] == "super"): 
        print(" "*4 + "c->Print(\"%s/plots/super_bg_sig.pdf[\",\"pdf\");"%(endpath))
        print 
        print(" "*4 + "TLegend *l;")
        
        for h in histos:
            for p in plots: 
                print(" "*4 + "%s_%s->GetXaxis()->SetTitle(\"%s\");"%(p, h[2].replace(' ',''),h[1]))

            # rebin 
            for p in plots:
                print(" "*4 + "grp = (1 + (int)log2(%s_%s->GetEntries()));"%(p, h[2].replace(' ','')))
                print(" "*4 + "%s_%s->Rebin(grp);"%(p, h[2].replace(' ','')))

            # set up line colours and legend 
            for p in plots:
                if("bg" in p):
                    print(" "*4 + "%s_%s->SetLineColor(kBlue);"%(p, h[2].replace(' ','')))
                    print(" "*4 + "%s_%s->Draw();"%(p, h[2].replace(' ','')))
                else:
                    print(" "*4 + "%s_%s->SetLineColor(kRed);"%(p, h[2].replace(' ','')))
                    print(" "*4 + "%s_%s->Draw(\"SAME\");"%(p, h[2].replace(' ','')))
            print(" "*4 + "c->Print();")
            print(" "*4 + "l = new TLegend(0.60,0.8,0.75,0.9);")
            for p in plots:
                if("bg" in p):
                    print(" "*4 + "l->AddEntry(%s_%s, \"Background\");"%(p,h[2].replace(' ','')))
                else:
                    print(" "*4 + "l->AddEntry(%s_%s, \"Signal\");"%(p,h[2].replace(' ','')))
            print(" "*4 + "l->Draw();")

            # rescale histograms and print to pdf file
            for p in plots:
                print(" "*4 + "%s_%s->Scale(1/%s_%s->Integral());"%(p, h[2].replace(' ',''), p, h[2].replace(' ','')))
                print(" "*4 + "%s_%s->GetYaxis()->SetTitle(\"Normalized count\");"%(p, h[2].replace(' ','')))
                print(" "*4 + "%s_%s->Draw(\"SAME\");"%(p, h[2].replace(' ','')))
            print(" "*4 + "c->Print(\"%s/plots/super_bg_sig.pdf\",\"Title: %s\");"%(endpath, h[2]))
            print(" "*4 + "c->Clear();")
            print

                
        for h_egg in histos_egammagamma: 
            for egg in egammagamma: 
                th1fname = h_egg[0] + " " + egg[1].replace('_',' ').title()
                for p in plots: 
                    print(" "*4 + "%s_%s->GetXaxis()->SetTitle(\"%s\");"%(p, th1fname.replace(' ',''), h_egg[1]))


                for p in plots: 
                    print(" "*4 + "grp = (1 + (int)log2(%s_%s->GetEntries()));"%(p, th1fname.replace(' ','')))
                    print(" "*4 + "%s_%s->Rebin(grp);"%(p, th1fname.replace(' ','')))


                for p in plots: 
                    if("bg" in p): 
                        print(" "*4 + "%s_%s->SetLineColor(kBlue);"%(p, th1fname.replace(' ','')))
                        print(" "*4 + "%s_%s->Draw();"%(p, th1fname.replace(' ','')))
                    else: 
                        print(" "*4 + "%s_%s->SetLineColor(kRed);"%(p, th1fname.replace(' ','')))
                        print(" "*4 + "%s_%s->Draw(\"SAME\");"%(p, th1fname.replace(' ','')))
                print(" "*4 + "c->Print();")
                print(" "*4 + "l = new TLegend(0.60,0.8,0.75,0.9);")
                for p in plots: 
                    if("bg" in p): 
                        print(" "*4 + "l->AddEntry(%s_%s, \"Background\");"%(p, th1fname.replace(' ','')))  
                    else: 
                        print(" "*4 + "l->AddEntry(%s_%s, \"Signal\");"%(p, th1fname.replace(' ','')))  
                print(" "*4 + "l->Draw();")


                for p in plots: 
                    print(" "*4 + "%s_%s->Scale(1/%s_%s->Integral());"%(p, th1fname.replace(' ',''), p, th1fname.replace(' ','')))
                    print(" "*4 + "%s_%s->GetYaxis()->SetTitle(\"Normalized count\");"%(p, th1fname.replace(' ','')))
                    print(" "*4 + "%s_%s->Draw(\"SAME\");"%(p, th1fname.replace(' ','')))
                print(" "*4 + "c->Print(\"%s/plots/super_bg_sig.pdf\",\"Title: %s\");"%(endpath, th1fname)) 
                print(" "*4 + "c->Clear();")
                print 
            print
        print(" "*4 + "c->Print(\"%s/plots/super_bg_sig.pdf)\",\"pdf\");"%(endpath))        
        print
    print("}")
   
    
# header mode generates the header file 
elif(sys.argv[1] == "header"):
    print("#ifndef PLOTS_H\n#define PLOTS_H\n")
    print("#include <vector>\n")
    print("using namespace std;\n")
    print("int grp;")
    print("TH1F *h;")
    for b in branches:
        print(" "*4 + "%s %s;"%(b[2],b[0]))
    print
    # get event histograms
    for h in histos:
        print(" "*4 + "TH1F *%s;"%h[2].replace(' ', ''))
    print
    # get histograms for each electron and photon
    # associated with the event
    for h_egg in histos_egammagamma:
        for egg in egammagamma:
            print(" "*4 + "TH1F *%s%s;"%(h_egg[0],(egg[1].replace('_',' ').title()).replace(' ','')))
    print

    print("#endif")


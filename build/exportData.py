import os

# we might be in a subdirectory
endpath = os.getcwd()[:os.getcwd().index("wggAnalysis")+11] 

# defines where to find output, build, and source files 
outpath = "%s/output"%(endpath)
buildpath = "%s/build"%(endpath)
eventsPassed = "%s/eventsPassed.root"%outpath

# all branches we need to get 
branches = [] 
vectorOrder = []

# get all relevant branches and information of vector order for photon/electron data 
with open("%s/config/cutpassdata.txt"%(buildpath)) as f:
    lines = list(f.readlines())
    branches = [line.strip('\n').split(',') for line in lines if '//' not in line and len(line.strip('\n').split(',')) == 3]
    f.close()
with open("%s/config/histos_egammagamma.txt"%(buildpath)) as f:
    lines = list(f.readlines())
    vectorOrder = [line.strip('\n').split(':')[1].split(',') for line in lines if "VECTOR_ORDER" in line and "//" not in line][0]
    f.close()
    
print("#include <iostream>")
print("#include <fstream>")
print
print("#include \"plots.h\"")
print
print
print("void exportData(){")
print

# get tree of events that have passed 
print(" "*4 + "TFile *ep = new TFile(\"%s\");"%(eventsPassed))
print(" "*4 + "TTree *evpass = (TTree *)ep->Get(\"eventsPassed\");")
print

# set up branches 
for b in branches:
    print(" "*4 + "evpass->SetBranchAddress(\"%s\",&%s);"%(b[1], b[0]))
print
print

# set up file
print(" "*4 + "ofstream epcsv(\"%s/eventsPassed.csv\");"%(outpath))
print

# headers for csv file 
headers = " "*4 + "epcsv "
for b in branches:
    if("photon" in b[1] or "electron" in b[1]):
        for v in vectorOrder: 
            headers += "<< \"%s_%s\" "%(v,b[1])
            if(vectorOrder.index(v) != len(vectorOrder) - 1):
                headers += " << \",\" "
    else:
       headers += "<< \"%s\" "%(b[1])
    if(branches.index(b) != len(branches) - 1): 
        headers += " << \",\" "
headers += " << \"\\n\";"
print(headers)
print


# print data to file 
print(" "*4 + "for (int i = 0; i < evpass->GetEntries(); i++){")
print
print(" "*8 + "evpass->GetEntry(i);")
print
line = " "*8 + "epcsv "
for b in branches:
    if("photon" in b[1] or "electron" in b[1]):
        for v in vectorOrder: 
            line += ("<< %s->at(%d) "%(b[0], vectorOrder.index(v)))
            if(vectorOrder.index(v) != len(vectorOrder) - 1):
                line += " << \",\" "
    else:
        line += ("<< %s "%(b[0]))
    if(branches.index(b) != len(branches)-1):
        line += " << \",\" "
line += " << \"\\n\";"
print(line)
print
print(" "*4 + "}")
print

print(" "*4 + "epcsv.close();")

print("}")



#include <iostream>
#include <fstream>

#include "plots.h"


void exportData(){

    TFile *ep = new TFile("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/output/eventsPassed.root");
    TTree *evpass = (TTree *)ep->Get("eventsPassed");

    evpass->SetBranchAddress("mcEventWeight",&mcEventWeight);
    evpass->SetBranchAddress("pileupWeight",&pileupWeight);
    evpass->SetBranchAddress("Wenu_mT",&Wenu_mT);
    evpass->SetBranchAddress("Wenu_phi",&Wenu_phi);
    evpass->SetBranchAddress("MET",&MET);
    evpass->SetBranchAddress("three_body_invariant_mass",&threeb_invmass);
    evpass->SetBranchAddress("two_body_invariant_mass",&twob_invmass);
    evpass->SetBranchAddress("delta_R_gamma_gamma",&dR_gg);
    evpass->SetBranchAddress("delta_R_e_gamma",&dR_eg);
    evpass->SetBranchAddress("lower_slice_bound",&slicelow);
    evpass->SetBranchAddress("higher_slice_bound",&slicehigh);
    evpass->SetBranchAddress("background_event",&bg_event);
    evpass->SetBranchAddress("leading_photon",&ppass_1);
    evpass->SetBranchAddress("second_leading_photon",&ppass_2);
    evpass->SetBranchAddress("leading_electron",&epass);


    ofstream epcsv("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/output/eventsPassed.csv");

    epcsv << "mcEventWeight"  << "," << "pileupWeight"  << "," << "Wenu_mT"  << "," << "Wenu_phi"  << "," << "MET"  << "," << "three_body_invariant_mass"  << "," << "two_body_invariant_mass"  << "," << "delta_R_gamma_gamma"  << "," << "delta_R_e_gamma"  << "," << "lower_slice_bound"  << "," << "higher_slice_bound"  << "," << "background_event"  << "," << "phi_leading_photon"  << "," << "eta_leading_photon"  << "," << "energy_leading_photon"  << "," << "pT_leading_photon"  << "," << "phi_second_leading_photon"  << "," << "eta_second_leading_photon"  << "," << "energy_second_leading_photon"  << "," << "pT_second_leading_photon"  << "," << "phi_leading_electron"  << "," << "eta_leading_electron"  << "," << "energy_leading_electron"  << "," << "pT_leading_electron"  << "\n";

    for (int i = 0; i < evpass->GetEntries(); i++){

        evpass->GetEntry(i);

        epcsv << mcEventWeight  << "," << pileupWeight  << "," << Wenu_mT  << "," << Wenu_phi  << "," << MET  << "," << threeb_invmass  << "," << twob_invmass  << "," << dR_gg  << "," << dR_eg  << "," << slicelow  << "," << slicehigh  << "," << bg_event  << "," << ppass_1->at(0)  << "," << ppass_1->at(1)  << "," << ppass_1->at(2)  << "," << ppass_1->at(3)  << "," << ppass_2->at(0)  << "," << ppass_2->at(1)  << "," << ppass_2->at(2)  << "," << ppass_2->at(3)  << "," << epass->at(0)  << "," << epass->at(1)  << "," << epass->at(2)  << "," << epass->at(3)  << "\n";

    }

    epcsv.close();
}

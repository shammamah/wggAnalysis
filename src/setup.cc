#include "setup.h"

void setup(){

    // storage for events that pass all cuts

    ep = new TFile("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/output/eventsPassed.root","recreate");
    evpass = new TTree("eventsPassed", "Events Passed");

    evpass->Branch("lower_slice_bound", &slicelow);
    evpass->Branch("leading_electron", "vector<double>", &epass);
    evpass->Branch("MET", &MET);
    evpass->Branch("second_leading_photon", "vector<double>", &ppass_2);
    evpass->Branch("higher_slice_bound", &slicehigh);
    evpass->Branch("background_event", &bg_event);
    evpass->Branch("mcEventWeight", &mcEventWeight);
    evpass->Branch("Wenu_mT", &Wenu_mT);
    evpass->Branch("two_body_invariant_mass", &twob_invmass);
    evpass->Branch("pileupWeight", &pileupWeight);
    evpass->Branch("three_body_invariant_mass", &threeb_invmass);
    evpass->Branch("leading_photon", "vector<double>", &ppass_1);
    evpass->Branch("delta_R_gamma_gamma", &dR_gg);
    evpass->Branch("delta_R_e_gamma", &dR_eg);
    evpass->Branch("Wenu_phi", &Wenu_phi);


    // slices

    l.Add(&bg_10_35);
    l.Add(&bg_35_70);
    l.Add(&bg_70_140);
    l.Add(&bg_140);
    l.Add(&sig_35_70);
    l.Add(&sig_70_140);


    // import data

    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000001.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000002.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000003.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000004.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000005.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000006.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000007.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000008.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000009.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000010.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000011.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000012.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000013.hist-output.root");
    bg_10_35.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301535-Pt_10_35/user.hrussell.13057452._000014.hist-output.root");



    bg_35_70.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301899-Pt_35_70/.DS_Store");
    bg_35_70.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301899-Pt_35_70/user.hrussell.13057484._000001.hist-output.root");



    bg_70_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301900-Pt_70_140/user.hrussell.13057496._000001.hist-output.root");
    bg_70_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301900-Pt_70_140/user.hrussell.13057496._000002.hist-output.root");
    bg_70_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagamma-301900-Pt_70_140/user.hrussell.13057496._000003.hist-output.root");



    bg_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagammaPt_140/user.hrussell.13057503._000001.hist-output.root");
    bg_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagammaPt_140/user.hrussell.13057503._000002.hist-output.root");
    bg_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/background/enugammagammaPt_140/user.hrussell.13057503._000003.hist-output.root");



    sig_35_70.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/signal/enu-munu-gammagamma-pty_35_70/munugammagamma_35_70_364577.root");
    sig_35_70.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/signal/enu-munu-gammagamma-pty_35_70/user.hrussell.13052664._000001.hist-output_364572.root");



    sig_70_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/signal/enu-munu-gammagamma_70_140/enugammagamma70140_364573.root");
    sig_70_140.Add("/Users/mammam/Dropbox/school/PHYS489/wggAnalysis/data/signal/enu-munu-gammagamma_70_140/munugammagamma70140_364578.root");



}


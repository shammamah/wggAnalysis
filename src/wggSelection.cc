#include <stdlib.h>
#include <math.h>
#include <string.h> 
#include <vector>

#include "../res/AtlasStyle.h"
#include "../res/AtlasUtils.h"
#include "../res/AtlasLabels.h"
#include "setup.h"

#ifdef __CLING__
#include "../res/AtlasLabels.C"
#include "../res/AtlasUtils.C"
#endif

using namespace std; 

void wggSelection(){
/*
 * Setup 
 */
    // ATLAS style 
    #ifdef __CINT__
      gROOT->LoadMacro("../res/AtlasLabels.C");
      gROOT->LoadMacro("../res/AtlasUtils.C"); 
    #endif

    // turn off verbose output 
    gErrorIgnoreLevel = kFatal;


/*
 * Initial import of data
 */
    gROOT->ProcessLine(".X setup.cc"); 

/*
 * Process each slice 
 */

    int indx = -1;

    TIter next(&l);
    while(TObject *obj = next()){

      indx++;
      printf("\nApplying cuts to %s...\n",slicenames[indx].c_str()); 

      /////////////////////////
      // set branch addresses
      /////////////////////////
      // event cuts 
      ((TChain *)obj)->SetBranchAddress("isGoodDataEvent",&isGoodDataEvent);
      ((TChain *)obj)->SetBranchAddress("hasGoodPV",&hasGoodPV);
      ((TChain *)obj)->SetBranchAddress("passTrig_e20_2g10",&passTrig_e20_2g10);
      // photon cuts 
      ((TChain *)obj)->SetBranchAddress("photon_eta",&pe);
      ((TChain *)obj)->SetBranchAddress("photon_pT",&ppT);
      ((TChain *)obj)->SetBranchAddress("photon_passIsolation",&ppI);
      ((TChain *)obj)->SetBranchAddress("photon_passKinCuts",&ppKC); 
      ((TChain *)obj)->SetBranchAddress("photon_passSelection",&ppS);
      // electron cuts
      ((TChain *)obj)->SetBranchAddress("electron_eta",&ee); 
      ((TChain *)obj)->SetBranchAddress("electron_pT",&epT);
      ((TChain *)obj)->SetBranchAddress("electron_passIsolation",&epI); 
      ((TChain *)obj)->SetBranchAddress("electron_passKinCuts",&epKC); 
      ((TChain *)obj)->SetBranchAddress("electron_passSelection",&epS);
      // event kinematic variables
      ((TChain *)obj)->SetBranchAddress("MET",&MET);
      ((TChain *)obj)->SetBranchAddress("Wenu_mT",&Wenu_mT);
      ((TChain *)obj)->SetBranchAddress("Wenu_phi",&Wenu_phi);
      // weights for histogram
      ((TChain *)obj)->SetBranchAddress("mcEventWeight",&mcEventWeight);
      ((TChain *)obj)->SetBranchAddress("pileupWeight",&pileupWeight);
      // invariant mass calculation 
      ((TChain *)obj)->SetBranchAddress("electron_E",&eE);
      ((TChain *)obj)->SetBranchAddress("photon_E",&pE);
      ((TChain *)obj)->SetBranchAddress("electron_phi",&ephi);
      ((TChain *)obj)->SetBranchAddress("photon_phi",&pphi);

      /////////////////////////
      // storage for events
      // that pass all cuts 
      /////////////////////////

      // TTree 
      char bg[20];  
      if(sscanf(slicenames[indx].c_str(), "%[^_]_%d_%d", bg, &slicelow, &slicehigh) < 3){
	sscanf(slicenames[indx].c_str(), "%*[^_]_%d", &slicelow);
	slicehigh = 0;
      } 
      const char *background = bg; 
      bg_event = (!strcmp(background, "background"));
      
      //////////////////////////
      // loop through data
      //////////////////////////

      for (int i=0;i<((TChain *)obj)->GetEntries();i++){
	((TChain *)obj)->GetEntry(i);

	// skip any entry that does not fulfil the boolean criteria
	if((isGoodDataEvent && hasGoodPV && passTrig_e20_2g10) == 0) {    
	  continue; 
	}
	// skip any entry that does not fulfil the MET or mT criteria 
	if(MET <= metMin || Wenu_mT <= wenumtMin) {
	  continue;
	}
	
	// PHOTON CUTS

	vector<double> ppass_e;
	vector<double> ppass_pT; 
	vector<int> p_indices;
	
	for (unsigned j = 0; j < (*ppS).size(); j++){
	  // basic cuts
	  if((ppI->at(j) && ppKC->at(j) && ppS->at(j)) == 0){
	    continue;
	  } 
	  // eta and pt cuts 
	  float eta = abs(pe->at(j));
	  float pt = ppT->at(j);
	  if((eta >= petaMax && eta <= petaMin1) || eta >= petaMax1){
	    continue;
	  }
	  if (pt <= pptMin){
	    continue;
	  }
	  // this photon has passed all cuts
	  ppass_e.push_back(pe->at(j));   // eta
	  ppass_pT.push_back(pt);         // pT 
	  p_indices.push_back(j); 
	}
	if(ppass_e.size() < 2){
	  continue;
	}
	
	// ELECTRON CUTS 

	vector<double> epass_e;
	vector<double> epass_pT;
	vector<int> e_indices; // to get energy and phi later  
	  
	for (unsigned j = 0; j < (*epS).size(); j++){
	  if((epI->at(j) && epKC->at(j) && epS->at(j)) == 0){
	    continue;
	  }
	  float eta = abs(ee->at(j));
	  float pt = epT->at(j);
	  if((eta >= eetaMax && eta <= eetaMin1) || eta >= eetaMax1){
	    continue;
	  }
	  if(pt <= eptMin){
	    continue;
	  }
	  epass_e.push_back(ee->at(j));
	  epass_pT.push_back(pt);
	  e_indices.push_back(j); 
	}
	if(epass_pT.size() < 1){
	  continue;
	} 
	else if(epass_pT.size() >= 2){
	  double max = *max_element(epass_pT.begin(), epass_pT.end());
	  double min = 0.0; 
	  if(epass_pT.size() == 2){
	    min = *min_element(epass_pT.begin(), epass_pT.end());
	  }
	  else if(epass_pT.size() > 2){
            for	(unsigned j = 0; j < epass_pT.size(); j++){
	      if(epass_pT.at(j) < max && epass_pT.at(j) > min){
		min = epass_pT.at(j); 
              }
            }
	  } 
	  if((max/min) < eptMin/eptIgnore){
	    continue;
	  }
	}

	// invariant mass calculation
	double px;
	double py;
	double pz; 
	double E;

	double invMass1;
	double invMass2; 
	
	TLorentzVector invMass_e;
	px = (epT->at(e_indices.at(0)))*(cos(ephi->at(e_indices.at(0))));
	py = (epT->at(e_indices.at(0)))*(sin(ephi->at(e_indices.at(0))));
	pz = (epT->at(e_indices.at(0)))*(cos(ee->at(e_indices.at(0))));
	E = eE->at(e_indices.at(0));
	invMass_e = invMass_e; 
	invMass_e.SetPxPyPzE(px, py, pz, E);
	
	TLorentzVector invMass_p1;
	px = (ppT->at(p_indices.at(0)))*(cos(pphi->at(p_indices.at(0))));
	py = (ppT->at(p_indices.at(0)))*(sin(pphi->at(p_indices.at(0))));
	pz = (ppT->at(p_indices.at(0)))*(cos(pe->at(p_indices.at(0))));
	E = pE->at(p_indices.at(0)); 
	invMass_p1.SetPxPyPzE(px, py, pz, E);
	invMass_p1 = invMass_p1; 
	invMass1 = (invMass_p1 + invMass_e).M();
	TLorentzVector invMass_p2;
	px = (ppT->at(p_indices.at(1)))*(cos(pphi->at(p_indices.at(1))));
	py = (ppT->at(p_indices.at(1)))*(sin(pphi->at(p_indices.at(1))));
	pz = (ppT->at(p_indices.at(1)))*(cos(pe->at(p_indices.at(1))));
	E = pE->at(p_indices.at(1));
	invMass_p2 = invMass_p2; 
        invMass_p2.SetPxPyPzE(px, py, pz, E);
        invMass2 = (invMass_p2 + invMass_e).M();
	
	// the two-body invariant mass that is closest
	// to the Z boson mass
	if(abs(invMass1 - zMass) < abs(invMass2 - zMass)){
	  twob_invmass = invMass1;
	}
	else{
	  twob_invmass = invMass2;
	}
	// the three-body invariant mass 
	threeb_invmass = (invMass_p1 + invMass_e + invMass_p2).M();

	// delta R for two photons 
	dR_gg = invMass_p1.DeltaR(invMass_p2); 
	// delta R for leading photon and electron
	dR_eg = invMass_p1.DeltaR(invMass_e);
	
	///////////////////////
	// create vectors 
	///////////////////////
	
	// photon 1
	ppass_1->push_back(pphi->at(p_indices.at(0)));
	ppass_1->push_back(pe->at(p_indices.at(0)));
	ppass_1->push_back(pE->at(p_indices.at(0)));
	ppass_1->push_back(ppT->at(p_indices.at(0)));
	// photon 2 
	ppass_2->push_back(pphi->at(p_indices.at(1)));	
        ppass_2->push_back(pe->at(p_indices.at(1)));
        ppass_2->push_back(pE->at(p_indices.at(1)));
        ppass_2->push_back(ppT->at(p_indices.at(1)));
	// electron 
	epass->push_back(ephi->at(e_indices.at(0)));
	epass->push_back(ee->at(e_indices.at(0)));
	epass->push_back(eE->at(e_indices.at(0)));
	epass->push_back(epT->at(e_indices.at(0)));

	///////////////////////
        // fill TTree 
        ///////////////////////  

	evpass->Fill();

	// clear the vectors so they can be reused 
	ppass_1->clear();
	ppass_2->clear();
	epass->clear(); 
      }

      printf("Done! \n"); 
  
    }

    ep->cd();
    evpass->Write(); 
}

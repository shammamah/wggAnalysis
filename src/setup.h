#ifndef SETUP_H
#define SETUP_H

#include <vector>

using namespace std;

    // for iterating through slices
    TList l;
    TIter next(&l);


    // slices
    const string slicenames[] = {
    "background_10_35",
    "background_35_70",
    "background_70_140",
    "background_140",
    "signal_35_70",
    "signal_70_140",
    };


    // storage of events that pass
    TFile *ep;
    TTree *evpass;


    // combined data for each slice
    TChain bg_10_35("recoTree");
    TChain bg_35_70("recoTree");
    TChain bg_70_140("recoTree");
    TChain bg_140("recoTree");
    TChain sig_35_70("recoTree");
    TChain sig_70_140("recoTree");


    // branches (
    vector<bool> * ppKC;
    vector<double> * ee;
    double pileupWeight;
    double mcEventWeight;
    vector<double> * pE;
    vector<double> * eE;
    vector<bool> * ppI;
    vector<bool> * ppS;
    bool isGoodDataEvent;
    vector<double> * pe;
    bool passTrig_e20_2g10;
    vector<bool> * epKC;
    vector<double> * epT;
    vector<double> * ppT;
    vector<bool> * epS;
    double Wenu_mT;
    vector<bool> * epI;
    bool hasGoodPV;
    double MET;
    vector<double> * ephi;
    vector<double> * pphi;
    double Wenu_phi;
    int slicelow;
    vector <double> * epass;
    vector <double> * ppass_2;
    int slicehigh;
    bool bg_event;
    double twob_invmass;
    double threeb_invmass;
    vector <double> * ppass_1;
    double dR_gg;
    double dR_eg;


    // cut parameters
    double metMin = 25.0/1000.0;
    double wenumtMin = 40.0/1000.0;
    float petaMax = 1.37; 
    float petaMin1 = 1.52; 
    float petaMax1 = 2.37; 
    float pptMin = 20.0; 
    float eptIgnore = 7;
    float eetaMax = 2.47; 
    float eetaMin1 = 1.37; 
    float eetaMax1 = 1.52; 
    float eptMin = 20.0; 
    double wMass = 80.4;
    double zMass = 91.2;
    double pMass = 0.0;
    double eMass = 0.000511; 


#endif

# Implement a basic neural network to predict background events.

import os
import sys 
import matplotlib.pyplot as plt
import numpy
import random
import time 
from datetime import datetime

###########################
# Setup
###########################

# we might be in a subdirectory 
endpath = os.getcwd()[:os.getcwd().index("wggAnalysis")+11]

# defines where to find output, build, and source files
outpath = "%s/output/"%(endpath)
buildpath = "%s/build"%(endpath) 
srcpath = "%s/src"%(endpath)
plotpath = "%s/plots/neunet"%(endpath)

# open data file 
eventsPassed = "%s/eventsPassed.csv"%outpath

# get all defined variables 
variables = [] 
with open (eventsPassed) as f:
    variables = list(f.readlines())[0].strip('\n').split(',')
    f.close()
testingVariable = "background_event"              # the variable we're trying to predict
samplingWeight = ["mcEventWeight","pileupWeight"] # weights for when we try to get a full sample

try:
    alphaIters = int(sys.argv[1])  # the number of times iterating until best alpha is found 
    weightIters = int(sys.argv[2]) # the number of times iterating until the optimal weights are found
except ValueError as e:
    print("Script failed; no valid integer values passed for alpha iters or weight iters.")
    print("Exiting script.")
    sys.exit()
    
useVars = []               # the variables that will be used in the analysis  
inputVars = sys.argv[3:]   # the variables we get as arguments      
nVars = len(inputVars)     # for checking if any of the input variables were 

if(inputVars[0] == "all"):
    useVars = [v for v in variables if v != testingVariable and v not in samplingWeight and "bound" not in v]

else:
    for i in inputVars:
        if i not in variables:
            nVars -= 1
            continue 
        useVars.append(i)
    if(len(useVars) != len(inputVars)):
        print("The following variables were ignored, because they do not correspond to the defined kinematic variables:")
        print
        for ignored in [i for i in inputVars if i not in useVars]: 
            print(ignored)
        print
    
if(nVars == 0):
    print("Script failed; no valid kinematic variables passed as parameters. \nThe list of valid kinematic variables is:")
    print
    print('-'*20)
    for v in variables:
        print(v)
    print('-'*20)
    print
    print("Exiting script.")
    sys.exit()
    
###########################
# Collect all data
###########################

# get all of the relevant training data from the full data set

def getTrainingData(varsToInclude):
    trainingDataFull = []
    trainingObsFull = [] 
    weightsFull = []
    
    kinVars = [] 
    
    mcWeightColumn = 0 
    pileupWeightColumn = 0 
    trainingObsColumn = 0    # row with the actual data (whether or not it's a background/signal event)
    
    # iterate through all variables in csv file  
    for v in variables:
        if(v not in varsToInclude and testingVariable not in v and v not in samplingWeight):
            continue
        if(v == testingVariable):
            trainingObsColumn = variables.index(v)
            continue
        if(v in samplingWeight):
            if(v == samplingWeight[0]):
                mcWeightColumn = variables.index(v)
                continue
            else:
                pileupWeightColumn = variables.index(v)
                continue
        kinVars.append(v)
    with open(eventsPassed) as f: 
        lines = list(f.readlines())[1:]
        for l in lines: 
            selectedData = []
            fullData = l.strip('\n').split(',')
            for k in kinVars:
                if('.' in fullData[variables.index(k)]):
                    selectedData.append(float(fullData[variables.index(k)]))
                else:
                    selectedData.append(int(fullData[variables.index(k)]))
            trainingDataFull.append(selectedData)
            trainingObsFull.append(int(fullData[trainingObsColumn]))
            weightsFull.append(float(fullData[mcWeightColumn])*float(fullData[pileupWeightColumn]))
        f.close()
    return(trainingDataFull, trainingObsFull, weightsFull)


###########################
# Ensures that we have the
# same amount of background
# and signal events
###########################

def fairSample(dataVars, dataObs, samplingWeights):
    # to split the data up 
    sig = []
    bg = []
    samplingWeightsSig = []
    samplingWeightsBg = []
    dataSig = []
    dataBg = [] 
    
    # initializing the lists that will become the arrays that are to be returned
    tmpObs = []
    tmpData = []

    # ensure we don't add the same event twice
    sigSampleIndices = []
    bgSampleIndices = [] 
    
    dataObsSig = []
    dataObsBg = [] 

    # to normalize the weights 
    maxWeight = max(samplingWeights)

    # splitting up into lists of signal and background events 
    for i in range(len(dataVars)):
        if(dataObs[i] == 0):
            sig.append(dataVars[i])
            samplingWeightsSig.append(samplingWeights[i])
            dataObsSig.append(dataObs[i])
        else:
            bg.append(dataVars[i])
            samplingWeightsBg.append(samplingWeights[i])
            dataObsBg.append(dataObs[i])
       
    # cycle through all the data until we have enough samples
    while(len(sigSampleIndices) < len(sig)/2):
        for i in range(len(sig)):     # get all the signal events
            if i in sigSampleIndices: # ignore events we've already added 
                continue 
            eventProb = samplingWeightsSig[i]/max(samplingWeightsSig)  # event probability 
            rand = numpy.random.random()
            if(rand >= eventProb):
                continue
            else:
                tmpData.append(sig[i])         # add data 
                tmpObs.append(dataObsSig[i])   # add result 
                sigSampleIndices.append(i)     # add to list of events added
                if(len(sigSampleIndices) == len(sig)/2):
                    break                      # exit when we have finished filling up the data 

    # same as above, but for background events 
    while(len(bgSampleIndices) < len(sig)/2):
        for i in range(len(bg)):     
            if i in bgSampleIndices: 
                continue
	    eventProb = samplingWeightsBg[i]/max(samplingWeightsBg)
            rand = numpy.random.random()
            if(rand >= eventProb):
          	continue
            else:
                tmpData.append(bg[i]) 
                tmpObs.append(dataObsBg[i])   
          	bgSampleIndices.append(i)
                if(len(bgSampleIndices) == len(sig)/2):
                    break 
    return (numpy.array(tmpData), numpy.array(tmpObs))


###########################
# Finds the learning rate
# based on when the weights
# converge the fastest
###########################

def findAlpha(dataVars, dataObs, alphaNorm, alphaUpdate, itersMax, plot):
    
    sumSqErrs = []
    alphas = []
    iterations = []
    weights = numpy.zeros(len(dataVars[0]))
    errors = numpy.zeros(dataObs.size)
    iters = 0
    while(iters < itersMax):
        alpha = numpy.multiply(alphaNorm, numpy.power(numpy.e,numpy.multiply(iters,alphaUpdate)))
        
        for i in range (dataObs.size):
            outcome = numpy.inner(weights,dataVars[i])
            errors[i] = numpy.subtract(dataObs[i], outcome)
            # the error associated with this dataset
            weights = numpy.add(weights, numpy.multiply(alpha,
                                            numpy.multiply(dataVars[i],errors[i])))
            # update all of the weights to suit this dataset 

        alphas.append(alpha)
        iterations.append(iters)
        sumSqErrs.append(numpy.inner(errors,errors))
        # conditions for stopping:
        # we have a satisfactory level of error,
        # or the error is too big
        
        if(numpy.inner(errors, errors) < numpy.power(epsilon,2)**dataVars.shape[0]
           or numpy.inner(errors, errors) > 10000):           
            break

        iters += 1

    # plots, if desired 
    if(plot == True):
        plt.figure(figsize = (20,10))
        fig, ax1 = plt.subplots()
        ax1.semilogy(iterations,sumSqErrs)
        plt.title("Error vs. Iterations in the calculation of alpha")
        plt.xlabel("Iterations")
        plt.ylabel("Norm of error vector")
        plt.savefig("%s/errs_vs_iters_alpha-%s"%(plotpath, datetime.now().strftime('%Y%m%d%H%M')))

    # return the alpha that led to the minimum error     
    return alphas[list(sumSqErrs).index(min(list(sumSqErrs)))]


###########################
# Single-perceptron
# neural network. 
###########################    

def neuralNet1(dataVars, dataObs, alpha, itersMax):
    normErrors = []
    iterations = [] 

    weights = numpy.random.rand(dataVars[0].size)
    errors = numpy.zeros(dataObs.size)

    iters = 0
    
    while(iters < itersMax):
        for i in range(dataObs.size):
            outcome = numpy.inner(weights, dataVars[i])
            errors[i] = numpy.subtract(dataObs[i], outcome)
            weights = numpy.add(weights, numpy.multiply(alpha,
                                            numpy.multiply(dataVars[i],errors[i])))
        if(numpy.inner(errors, errors) < epsilon):
            break
        iterations.append(iters)
        normErrors.append(numpy.inner(errors, errors))
        iters += 1

    return (weights, iterations, normErrors) 


###########################
# Multi-layer feed-forward
# neural network
###########################

# helper function: calculate activation
def activation(weights, inputs, fn = 'sigmoid'):
    fnInput = numpy.inner(weights, inputs)
    if(fn == "sigmoid"):
        return numpy.divide(1.0,numpy.add(1.0, numpy.power(numpy.e, numpy.multiply(-1.0,fnInput))))
    
# helper function: calculate gradient of activation 
def gradActivation(weights, inputs, fn = 'sigmoid'):
    if(fn == "sigmoid"):
        return numpy.multiply(activation(weights,inputs,"sigmoid"),numpy.subtract(1.0,activation(weights,inputs,"sigmoid")))

# two-layer neural network
def neuralNet2(dataVars, dataObs, alpha, itersMax, nHidden): 
    iterations = []
    normErrors = [] 

    hiddenNodes = [numpy.zeros(nHidden) for i in range(dataObs.size)]
    # storage for values of hidden nodes 
    output = numpy.zeros(dataObs.size)
    
    # the values associated with each hidden node
    output_hidden_weights = numpy.random.rand(nHidden)
    # weights for hidden nodes to output 
    hidden_input_weights = [numpy.random.rand(dataVars[0].size) for j in range(nHidden)]
    # weights for input nodes to hidden nodes
    
    Ai = 0.0
    Aj = numpy.zeros(nHidden)
    # error multiplied by gradient
    
    output_hidden_errors = numpy.zeros(dataObs.size)
    # errors associated with hidden nodes and output 

    iters = 0 
    
    while(iters < itersMax):
        
        # do for all examples 
        for i in range(dataObs.size): 

            # run the network 
            for j in range(nHidden):
                # compute the activations of the hidden units
                hiddenNodes[i][j] = activation(hidden_input_weights[j],    # weights 
                                               dataVars[i])                # inputs 
                                                                           # (function type)
                                                                           
            # now that the activations are computed, compute the output node
            output[i] = numpy.inner(output_hidden_weights, hiddenNodes[i])
            # compute the errors
            output_hidden_errors[i] = dataObs[i] - output[i]
            Ai = numpy.multiply(output_hidden_errors[i], gradActivation(output_hidden_weights, hiddenNodes[i]))

            
            # update weights from hidden to output
            for j in range(nHidden):
                output_hidden_weights = numpy.add(output_hidden_weights, numpy.multiply(alpha,
                                                                numpy.multiply(activation(hidden_input_weights[j], dataVars[i]),
                                                                               Ai)))

                Aj[j] = numpy.multiply(gradActivation(output_hidden_weights, hiddenNodes[i]),
                                  numpy.multiply(output_hidden_weights[j], Ai))
                # update weights from input to hidden 
                for k in range(dataVars[0].size): 
                    hidden_input_weights[j][k] = numpy.add(hidden_input_weights[j][k],
                                                        numpy.multiply(alpha,
                                                                       numpy.multiply(dataVars[i][k],
                                                                                      Aj[j])))

        if(numpy.inner(output_hidden_errors,output_hidden_errors) < epsilon*(dataVars[0].size)**2):
            break
        alpha = 0.0001*numpy.power(numpy.e,0.001*iters)
        iterations.append(iters)
        normErrors.append(numpy.inner(output_hidden_errors, output_hidden_errors))
        iters += 1
        
    return(output_hidden_weights,hidden_input_weights,iterations,normErrors)


###########################
# Setup 
###########################

epsilon = 0.005
testNumber = 0 
portionCorrect = []    # the portion of events correctly identified
fakeRate = []          # the portion of background events misidentified as signal
signalEfficiency = []  # the portion of signal events correctly identified as signal

fig = plt.figure(figsize = (12,8))

# get all relevant data 
(trainingDataFull, trainingObsFull, samplingWeights) = getTrainingData(useVars)

print("Kinematic variables:")
print("-"*20)
for var in useVars:
    print(var)
print("-"*20)

(trainingData, trainingObs) = fairSample(trainingDataFull, trainingObsFull, samplingWeights)
trainingNorm = 1.0/(numpy.inner(trainingData[0], trainingData[0])*trainingData.shape[0])
trainingAlpha = findAlpha(trainingData, trainingObs, trainingNorm, 0.01, alphaIters, True)

###########################
# Neural networks
###########################

# Single-perceptron network 
def runSingleNetwork(plot, printout, iters): 

    plt.figure(figsize=(20,10))
    
    portionCorrect = []    # the portion of events correctly identified
    fakeRate = []          # the portion of background events misidentified as signal
    signalEfficiency = []  # the portion of signal events correctly identified as signal

    start = time.time()
    for n in range(iters):
        # run the network
        # time it!
        (weights,iterations,normErrors) = neuralNet1(trainingData, trainingObs, trainingAlpha, weightIters)

        # determine fake rate, error rate, and signal efficiency 
        weights = numpy.array(weights)
        err = 0
        fakerate = 0
        signalefficiency = 0 
        for i in range(trainingData.shape[0]):
            outcome = round(numpy.inner(weights, trainingData[i]))
            if (outcome - trainingObs[i] != 0):
                err += 1
            if(outcome == 0 and trainingObs[i] == 1):
                fakerate +=1
            if(outcome == 0 and trainingObs[i] == 0):
                signalefficiency += 1

        # record this information 
        portionCorrect.append(1.0 - float(err)/float(trainingData.shape[0]))
        fakeRate.append(float(fakerate)/float(trainingData.shape[0]))
        signalEfficiency.append(float(signalefficiency)/float(trainingData.shape[0]))

        # plot, if desired 
        if(plot):
            plt.semilogy(iterations, normErrors)

    # print out timing info 
    print("%d seconds taken to run %d iterations."%((time.time() - start), iters))

    # print out values, if desired 
    if(printout): 
        for p in portionCorrect:
            print(p)
        print
        print('-'*20)
        for f in fakeRate:
            print(f)
        print
        print('-'*20)
        for s in signalEfficiency:
            print(s)
        print('-'*20)

    # print out statistics 
    print("Average proportion of error (single layer network): " + str(numpy.mean(portionCorrect)))
    print("Average fake rate (single layer network): " + str(numpy.mean(fakeRate)))
    print("Average signal efficiency (single layer network): " + str(numpy.mean(signalEfficiency)))
    print

    # plot, if desired 
    if(plot):
        plt.xlabel("Number of iterations",fontsize=20)
        plt.ylabel("Norm of error vector",fontsize=20)
        plt.title("Errors for a Single-Perceptron Neural Network",fontsize=24)
        plt.tick_params(axis='both', which='major', labelsize=16)
        plt.savefig("%s/errs_iters_single_%s"%(plotpath, datetime.now().strftime('%H%M')))

        
# multi-level neural network         
def runMultiNetwork(plot, printout, iters, nHidden):

    plt.figure(figsize=(16,10))
   
    
    portionCorrect = []    
    fakeRate = []          
    signalEfficiency = []  

    start = time.time()
    
    for n in range(iters):
        print("iteration %d"%n)
        # the training alpha here does not apply; start with a base value
        # of 0.001 and increment exponentially according to the iteration
        # in the network 
        (outputHiddenWeights, hiddenInputWeights, iterations, normErrors) = neuralNet2(trainingData, trainingObs, 0.001, weightIters, nHidden)

        # weights between output node and hidden nodes 
        outputHiddenWeights = numpy.array(outputHiddenWeights)
        # weights between hidden nodes and input nodes 
        hiddenInputWeights = numpy.array(hiddenInputWeights)
        # storage for values of hidden nodes 
        hiddenNodes = numpy.zeros(outputHiddenWeights.size)

        err = 0
        fakerate = 0
        signalefficiency = 0

        # calculation of outcome involves activatio of all
        # hidden nodes 
        for i in range(trainingData.shape[0]):
            for j in range(hiddenNodes.size): 
                hiddenNodes[j] = activation(hiddenInputWeights[j], trainingData[i])
            outcome = round(numpy.inner(hiddenNodes, outputHiddenWeights))

            if(outcome - trainingObs[i] != 0):
                err += 1
            if(outcome == 0 and trainingObs[i] == 1):
                fakerate += 1
            if(outcome == 0 and trainingObs[i] == 0):
                signalefficiency += 1
        portionCorrect.append(1.0 - float(err)/float(trainingData.shape[0]))
        fakeRate.append(float(fakerate)/float(trainingData.shape[0]))
        signalEfficiency.append(float(signalefficiency)/float(trainingData.shape[0]))

        if(plot):
            # plot all on comparable axes to see differences properly 
            plt.ylim((0, 10000))
            plt.semilogy(iterations, normErrors)
            
    print("%d seconds taken to perform %d iterations."%((time.time() - start),iters)) 
            
    if(printout):
        for p in portionCorrect:
            print(p)
        print
        print('-'*20)
        for f in fakeRate:
            print(f)
        print
        print('-'*20)
        for s in signalEfficiency:
            print(s)
        print('-'*20)
    print("Average proportion of correct prediction (multi layer network): " + str(numpy.mean(portionCorrect)))
    print("Average fake rate (multi layer network): " + str(numpy.mean(fakeRate)))
    print("Average signal efficiency (multi layer network): " + str(numpy.mean(signalEfficiency)))
    print

    if(plot): 
        plt.xlabel("Number of iterations", fontsize=20)
        plt.ylabel("Norm of error vector", fontsize=20)
        plt.tick_params(axis='both', which='major', labelsize=20)
        plt.tick_params(axis='both', which='minor', labelsize=20)
        plt.title("Errors for a Multi-Layered Feed-Forward Network (%d Hidden Nodes)"%nHidden, fontsize=24) 
        plt.savefig("%s/errs_iters_multi_%d"%(plotpath, nHidden))

        
######################
# Cool stuff below!
######################

#runSingleNetwork(True, False, 10)
#runMultiNetwork(True, True, 10, 5)


#ifndef PLOTS_H
#define PLOTS_H

#include <vector>

using namespace std;

int grp;
TH1F *h;
    double mcEventWeight;
    double pileupWeight;
    double Wenu_mT;
    double Wenu_phi;
    double MET;
    double threeb_invmass;
    double twob_invmass;
    double dR_gg;
    double dR_eg;
    int slicelow;
    int slicehigh;
    bool bg_event;
    vector <double> * ppass_1;
    vector <double> * ppass_2;
    vector <double> * epass;

    TH1F *ThreeBodyInvariantMass;
    TH1F *TwoBodyInvariantMass;
    TH1F *DeltaRLeadingPhotonandElectron;
    TH1F *DeltaRLeadingPhotons;
    TH1F *MissingTransverseEnergy;
    TH1F *WenuTransverseMass;
    TH1F *WenuPhi;

    TH1F *pTLeadingPhoton;
    TH1F *pTSecondLeadingPhoton;
    TH1F *pTLeadingElectron;
    TH1F *etaLeadingPhoton;
    TH1F *etaSecondLeadingPhoton;
    TH1F *etaLeadingElectron;

#endif

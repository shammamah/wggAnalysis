# automatically tests the single-layer perceptron network
# with different combinations of the selected kinematic
# variables. 

import os

vars = ["two_body_invariant_mass",
        "three_body_invariant_mass",
        "pT_leading_photon",
        "MET",
        "delta_R_e_gamma",
        "delta_R_gamma_gamma"]

for i in range(1, 2**len(vars)):
    string = ('{0:06b}'.format(i))
    cmd = "python predictBackground.py 1000 1000 "
    for j in range(1,len(vars)):
        if(string[j] == '1'):
            cmd += vars[j] + " "
    print(string + " & ")
    os.system(cmd)
